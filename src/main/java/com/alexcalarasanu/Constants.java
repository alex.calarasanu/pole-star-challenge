package com.alexcalarasanu;

public  class Constants {
    private Constants(){}
    final static String INPUT_QUESTION = "Please input the input file name (input.json): ";
    final static String FILE_NOT_FOUND_OR_BAD_FORMAT = "File not found, or it is not a valid JSON file please try again: ";
    final static String OUTPUT_QUESTION = "Please input the output file name (output.sql): ";
    final static String FILE_CREATED = "-----------------FILE CREATED-----------------\n";
    final static String FILE_NOT_CREATED = "-----------------FILE NOT CREATED-----------------\n";
}
