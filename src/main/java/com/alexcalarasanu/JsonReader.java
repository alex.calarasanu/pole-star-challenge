package com.alexcalarasanu;

import com.alexcalarasanu.model.Site;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;

public class JsonReader {

    public Site[] readJSON(InputStream inputStream) {

        ObjectMapper mapper = new ObjectMapper();
        Site[] sites;
        try {
            sites = mapper.readValue(inputStream, Site[].class);
        } catch (IOException e) {
            return null;
        }


        return sites;
    }

}
