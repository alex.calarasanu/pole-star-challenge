package com.alexcalarasanu;

import com.alexcalarasanu.model.Parameter;
import com.alexcalarasanu.model.Site;

public class SqlBuilder {
    private final String CREATE_TABLES_SQL = "CREATE TABLE sites (\n" +
            "UniqueID VARCHAR(50),\n" +
            "Name VARCHAR(50),\n" +
            "AlarmColor INT (10),\n" +
            "Id int(10),\n" +
            "DatasourcesCount INT(10),\n" +
            "_alertIcon VARCHAR(50),\n" +
            "ElementCount INT(10),\n" +
            "PRIMARY KEY (UniqueID)\n" +
            ");\n" +
            "\n" +
            "CREATE TABLE parameters (\n" +
            "_Key VARCHAR(50),\n" +
            "_Value VARCHAR(50),\n" +
            "UniqueID VARCHAR(50),\n" +
            "FOREIGN KEY (UniqueID) REFERENCES sites(UniqueID)\n" +
            ");\n";

    public String buildSql(Site[] sites) {
        StringBuilder sqlBuilder = new StringBuilder();

        sqlBuilder.append(CREATE_TABLES_SQL);

        for (Site site : sites) {
            sqlBuilder.append("insert into sites values(\"")
                    .append(site.getUniqueID())
                    .append("\",\"")
                    .append(site.getName())
                    .append("\",")
                    .append(site.getAlarmColor())
                    .append(",")
                    .append(site.getId())
                    .append(",")
                    .append(site.getDatasourcesCount())
                    .append(",\"")
                    .append(site.get_alertIcon())
                    .append("\",")
                    .append(site.getElementCount())
                    .append(");\n");
            for (Parameter parameter : site.getParameter())
                sqlBuilder.append("insert into parameters values(\"")
                        .append(parameter.getKey())
                        .append("\",\"")
                        .append(parameter.getValue())
                        .append("\",\"")
                        .append(site.getUniqueID())
                        .append("\");\n");
        }
        return sqlBuilder.toString();
    }
}
