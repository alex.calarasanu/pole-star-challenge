package com.alexcalarasanu;

import com.alexcalarasanu.model.Site;

import java.io.*;
import java.nio.charset.StandardCharsets;

import static com.alexcalarasanu.Constants.*;

public class Main {

    public static void main(String[] args) {
        Console console = System.console();
        String inputFile="";
        String outputFile="";
        Site[] sites = null;

        if(console!=null){
        inputFile = console.readLine(INPUT_QUESTION);
        System.out.println("Name entered: " + inputFile);
        }
        JsonReader reader = new JsonReader();
        InputStream inputStream = Main.class.getResourceAsStream("/" + inputFile);
        while (sites == null) {
            sites = reader.readJSON(inputStream);
            if (sites == null) {
                inputFile = console.readLine(FILE_NOT_FOUND_OR_BAD_FORMAT);
                System.out.println("Name entered: " + inputFile);
                inputStream = Main.class.getResourceAsStream("/" + inputFile);
            }
        }

        SqlBuilder sqlBuilder = new SqlBuilder();
        String sqlString = sqlBuilder.buildSql(sites);
        if(console!=null) {
           outputFile = console.readLine(OUTPUT_QUESTION);
        }
        if (!outputFile.isEmpty()) {
            try (Writer writer = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(outputFile), StandardCharsets.UTF_8))) {
                writer.write(sqlString);
                System.out.println(FILE_CREATED + outputFile);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            System.out.println(FILE_NOT_CREATED + sqlString);
        }

    }
}
