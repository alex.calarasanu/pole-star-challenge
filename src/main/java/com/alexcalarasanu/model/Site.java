package com.alexcalarasanu.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class Site {

    @JsonProperty("Parameters")
    List<Parameter> parameters;

    @JsonProperty("Name")
    private String name;

    @JsonProperty("AlarmColor")
    private int alarmColor;

    @JsonProperty("Id")
    private int id;

    @JsonProperty("DatasourcesCount")
    private int datasourcesCount;

    @JsonProperty("_alertIcon")
    private String _alertIcon;

    @JsonProperty("ElementCount")
    private int elementCount;

    @JsonProperty("UniqueID")
    private String uniqueID;

    public Site() {
    }

    public Site(String name, int alarmColor, int id, List<Parameter> parameters, int datasourcesCount, String _alertIcon, int elementCount, String uniqueID) {
        this.name = name;
        this.alarmColor = alarmColor;
        this.id = id;
        this.parameters = parameters;
        this.datasourcesCount = datasourcesCount;
        this._alertIcon = _alertIcon;
        this.elementCount = elementCount;
        this.uniqueID = uniqueID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAlarmColor() {
        return alarmColor;
    }

    public void setAlarmColor(int alarmColor) {
        this.alarmColor = alarmColor;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Parameter> getParameter() {
        return parameters;
    }

    public void setParameter(List<Parameter> parameter) {
        this.parameters = parameter;
    }

    public int getDatasourcesCount() {
        return datasourcesCount;
    }

    public void setDatasourcesCount(int datasourcesCount) {
        this.datasourcesCount = datasourcesCount;
    }

    public String get_alertIcon() {
        return _alertIcon;
    }

    public void set_alertIcon(String _alertIcon) {
        this._alertIcon = _alertIcon;
    }

    public int getElementCount() {
        return elementCount;
    }

    public void setElementCount(int elementCount) {
        this.elementCount = elementCount;
    }

    public String getUniqueID() {
        return uniqueID;
    }

    public void setUniqueID(String uniqueID) {
        this.uniqueID = uniqueID;
    }
}
