package com.alexcalarasanu;

import com.alexcalarasanu.model.Parameter;
import com.alexcalarasanu.model.Site;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;

@RunWith(JUnit4.class)
public class JsonReaderTest {


    String jsonString = "[{\n" +
            "  \"Name\": \"Site 1\",\n" +
            "  \"AlarmColor\": -1671296,\n" +
            "  \"Id\": 8,\n" +
            "  \"Parameters\": [{\n" +
            "    \"Key\": \"Name\",\n" +
            "    \"Value\": \"Site 1\"\n" +
            "  }],\n" +
            "  \"DatasourcesCount\": 0,\n" +
            "  \"_alertIcon\": \"Communications\",\n" +
            "  \"ElementCount\": 640,\n" +
            "  \"UniqueID\": \"87111c51-08df-4b29-85c5-43803a994bdd\"\n" +
            "}]";
    JsonReader jsonReader = new JsonReader();

    @Test
    public void testReadJsonGreenPath(){
        Site[] result = jsonReader.readJSON(new ByteArrayInputStream(jsonString.getBytes()));
        Site[] expected = buildSites();
        assertEquals(expected[0].getUniqueID(),result[0].getUniqueID());
    }

    @Test
    public void testReadJsonRedPath(){
        Site[] result = jsonReader.readJSON(new ByteArrayInputStream(("invalid").getBytes()));
        assertNull(result);
    }

    private Site[] buildSites(){
        Site[] sites = new Site[1];
        Parameter parameter = new Parameter("Name","Site 1");
        List<Parameter> parameters = new ArrayList<>();
        parameters.add(parameter);
        Site site = new Site("Site 1",-1671296,8,parameters,0,"Communications",640,"87111c51-08df-4b29-85c5-43803a994bdd");
        sites[0] = site;
        return sites;
    }

}
