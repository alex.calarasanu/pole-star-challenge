package com.alexcalarasanu;

import com.alexcalarasanu.model.Parameter;
import com.alexcalarasanu.model.Site;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class SqlBuilderTest {
    SqlBuilder sqlBuilder = new SqlBuilder();

    @Test
    public void testBuildString(){
        buildSites();
        String expected = "CREATE TABLE sites (\n" +
                "UniqueID VARCHAR(50),\n" +
                "Name VARCHAR(50),\n" +
                "AlarmColor INT (10),\n" +
                "Id int(10),\n" +
                "DatasourcesCount INT(10),\n" +
                "_alertIcon VARCHAR(50),\n" +
                "ElementCount INT(10),\n" +
                "PRIMARY KEY (UniqueID)\n" +
                ");\n" +
                "\n" +
                "CREATE TABLE parameters (\n" +
                "_Key VARCHAR(50),\n" +
                "_Value VARCHAR(50),\n" +
                "UniqueID VARCHAR(50),\n" +
                "FOREIGN KEY (UniqueID) REFERENCES sites(UniqueID)\n" +
                ");\n" +
                "insert into sites values(\"87111c51-08df-4b29-85c5-43803a994bdd\",\"Site 1\",-1671296,8,0,\"Communications\",640);\n" +
                "insert into parameters values(\"Name\",\"Site 1\",\"87111c51-08df-4b29-85c5-43803a994bdd\");\n";
        assertEquals(expected,sqlBuilder.buildSql(buildSites()));
    }
    private Site[] buildSites(){
        Site[] sites = new Site[1];
        Parameter parameter = new Parameter("Name","Site 1");
        List<Parameter> parameters = new ArrayList<>();
        parameters.add(parameter);
        Site site = new Site("Site 1",-1671296,8,parameters,0,"Communications",640,"87111c51-08df-4b29-85c5-43803a994bdd");
        sites[0] = site;
        return sites;
    }
}
